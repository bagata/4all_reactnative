/**
 *
 * This is the details screen
 *  From photo to comments, this screen is a mix of a lot o components
 *  Divided into top, main content and bottom
 *
 * @summary Details screen that contains a place data
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import { ActivityIndicator, Image, Alert, Dimensions, Text, ScrollView, View, Linking } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MenuItem from '../MenuItem';
import Map from '../CustomMap';
import Comment from '../Comment';
import colors from '../../styles/colors';
import styles from './styles';

export default class DetailsScreen extends React.Component {

  // Header Options
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title'),
      headerStyle: {
        backgroundColor: colors.primary,
      },
      headerTintColor: colors.secondary
    };
  };

  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      place: null
    }
  }

  componentDidMount = () => {
    // Gets place id param from previous screen 
    const placeId =  this.props.navigation.getParam('id');

    // Makes a request to get place data
    this.getPlaceDetails(placeId);
  }

  /**
   * Request to get place data (JSON)
   */
  getPlaceDetails = (id) => {
    if(!!id) {
      fetch('http://dev.4all.com:3003/tarefa/'+id)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          place: responseJson,
        }),
        this.props.navigation.setParams({
          'title': `${responseJson.cidade} - ${responseJson.bairro}`
        })
      })
      .catch((error) => {
        this.setState({
          isLoading: false
        });
        console.error(error);
      });
    }
  }

  /**
   * Opens system dial with given phone number
   * @param {*} phoneNumber 
   */
  openDial = (phoneNumber) => {
    if(!!phoneNumber) {
      Linking.openURL(`tel:${phoneNumber}`);
    }
  }

  /**
   *  Uses react navigation to display given screen
   * @param {string} screen 
   */
  goToScreen = (screen) => {
    if(!!screen) {
      this.props.navigation.push(screen)
    }
  }

  /**
   * Displays an alert with the address information
   */
  openAddressAlert = () => {
    Alert.alert(
      'Endereço',
      this.state.place.endereco,
      [
        {text: 'Fechar', onPress: () => console.log('Closed')},
      ],
      {cancelable: false},
    );
  }

  /**
   * Gets screen height and then scrolls to the bottom (where the comments section is)
   */
  scrollToComments = () => {
    const height = Dimensions.get('window').height;
    this.scrollView.scrollTo({y:height});
  }

  render = () => {
    const place = this.state.place;
    const comments = !!place ? place.comentarios : [];
    // Shows loading spinner while request is being made
    if(this.state.isLoading) {
      return <ActivityIndicator size="large" color="#CB8A19" style={styles.activityIndicator} />
    }
    else if(!!place) {
      return (
        <ScrollView ref={view => this.scrollView = view}>
          <View style={styles.container}>
  
            {/* Top content */}
            <View style={styles.topContent}>
              <Image source={{uri: !!place.urlFoto ? place.urlFoto : ''}} style={{ width:Dimensions.get('window').width, height: 220 }} />
              <View style={styles.starIconBorder}>
                <FontAwesome
                  name={'star'}
                  color="#CB8A19"
                  size={70}
                  style={styles.starIcon}
                />
              </View>
              <Text style={styles.title}>{!!place.titulo ? place.titulo : ''}</Text>
            </View>
  
            {/* Main content */}
            <View style={styles.mainContent}>
              <View style={styles.menu}>
                <MenuItem iconName={'phone'} label={'Ligar'} size={20} action={() => this.openDial(place.telefone)} />
                <MenuItem iconName={'diamond'} label={'Serviços'} size={20} action={() => this.goToScreen('Services')} />
                <MenuItem iconName={'map-marker'} label={'Endereço'} size={20} action={() => this.openAddressAlert()} />
                <MenuItem iconName={'comments'} label={'Comentários'} size={20} action={() => this.scrollToComments()} />
                <MenuItem iconName={'star'} label={'Favoritos'} size={20} />
              </View>
              <View>
                <Text style={styles.mainText}>{!!place.texto ? place.texto : ''}</Text>
              </View>
            </View>
  
            {/* Bottom content */}
            <View style={styles.bottomContent}>
              <Map latitude={place.latitude} longitude={place.longitude} address={place.endereco} />
              {
                !!comments ? 
                comments.map((comment, index) => {
                  return <Comment key={index} data={comment}/>
                })
                : null
              }
            </View>
  
          </View>
        </ScrollView>
      );
    }
    else { 
      return null;
    }
  }
}