import { Dimensions, StyleSheet } from 'react-native';
import colors from '../../styles/colors';

const styles = StyleSheet.create({
  activityIndicator: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
    backgroundColor: colors.alternative
  },
  starIconBorder: {
    position: 'absolute',
    top: 180,
    left: Dimensions.get('window').width - 120,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    overflow: "hidden",

  },
  starIcon: {
    width: 90,
    height: 90,
    lineHeight: 90,
    textAlign: 'center',
    backgroundColor: colors.secondary
  },
  title: {
    color: colors.primary,
    fontSize: 30,
    marginVertical: 10,
    marginHorizontal: 20,
  },
  mainContent: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: colors.secondary,
    paddingHorizontal: 10
  },
  menu: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderBottomColor: colors.alternative,
    borderBottomWidth: 1,
    paddingVertical: 10
  },
  mainText: {
    color: colors.primary,
    fontSize: 16,
    padding: 10,
    textAlign: 'left'
  }
});
export default styles;