/**
 *
 * Gets an image from Google Maps API for given latitude and longitude data
 * 
 * @summary Google Maps Picture
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import { View, Text } from 'react-native';
import FullWidthImage from 'react-native-fullwidth-image';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import styles from './styles';
import { GOOGLE_MAPS_API_KEY } from 'react-native-dotenv';

const CustomMap = ({ latitude, longitude, address }) => {
  return (
    <View style={styles.container}>
    {/* Google maps image with full width */}
      <FullWidthImage style={styles.map} source={{uri: `https://maps.googleapis.com/maps/api/staticmap?center=${latitude},${longitude}&zoom=15&size=800x150&zoom=2&markers=size:mid%7Ccolor:red%7C${latitude},${longitude}&key=${GOOGLE_MAPS_API_KEY}`}} />
      <View style={styles.addressWrap}>
        <Text style={styles.address}>{address}</Text>
        <View style={styles.mapIconBorder}>
          <FontAwesome
            name={'map-marker'}
            color="#CB8A19"
            size={20}
            style={styles.mapIcon}
          />
        </View>
      </View>
    </View>
  )
}

export default CustomMap;