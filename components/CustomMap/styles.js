import { StyleSheet } from 'react-native';
import colors from '../../styles/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    height: 100
  },
  addressWrap: {
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'flex-end',
    height: 20,
    paddingHorizontal: 10,
    backgroundColor: colors.primary,
    paddingHorizontal: 10
  },
  address: {
    color: colors.secondary,
    textAlign: 'right',
    paddingHorizontal: 20
  },
  mapIconBorder: {
    paddingBottom: 15,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
  },
  mapIcon: {
    width: 30,
    height: 30,
    lineHeight: 30,
    borderRadius: 50,
    textAlign: 'center',
    backgroundColor: colors.secondary
  }
});
export default styles;