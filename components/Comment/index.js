/**
 *
 * Comment layout with stars rating system
 * 
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import { Text, Image, View } from 'react-native';
import styles from './styles';
import Rating from '../Rating';
import colors from '../../styles/colors';

const Comment = ({ data }) => {
  
  if(!!data) {
    return (
      <View style={styles.container}>
        <View style={styles.wrap}>
          {/* First column for comment picture/avatar */}
          <View style={[styles.col, styles.commentPicture]}>
            <View style={styles.avatarBorder}>
              <Image style={styles.avatar} source={{uri: data.urlFoto }} />
            </View>
          </View>
          {/* Second column for comment content and rating  */}
          <View style={[styles.col, styles.commentContent]}>
            <View style={styles.row}>
              <View style={styles.col}>
                <View style={styles.row}>
                  <Text style={styles.commentText}>{data.nome}</Text>
                </View>
                <View style={styles.row}>
                  <Text style={styles.commentText}>{data.titulo}</Text>
                </View>
              </View>
              <View style={[styles.col, styles.starsWrap]}>
                <Rating stars={data.nota} color={colors.primary}/>
              </View>
            </View>
            <View style={styles.row}>
              <Text style={styles.commentText}>{data.comentario}</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
  else {
    return null;
  }
};

export default Comment;