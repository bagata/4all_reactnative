import colors from '../../styles/colors';
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.secondary
  },
  wrap: {
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start'
  },
  row: {
    flexDirection: 'row',
  },
  col: {
    flexDirection: 'column',
  },
  commentPicture: {
    flexDirection: 'row',
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
  },
  avatarBorder: {
    width: 65,
    height: 65,
    borderRadius: 50,
    overflow: "hidden",
  },
  avatar: {
    width: 65,
    height: 65,
  },
  commentContent: {
    flex: 7,
    justifyContent: 'flex-start'
  },
  commentText: {
    color: colors.primary,
    fontSize: 14,
    flexShrink: 1,
    paddingVertical: 2
  },
  starsWrap: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
});
export default styles;