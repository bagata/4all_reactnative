/**
 *
 * This is the services screen. Doesn't have any content but the header.
 *
 * @summary Empty screen
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import colors from '../../styles/colors';

export default class ServicesScreen extends React.Component {

  // Header Options
  static navigationOptions = () => {
    return {
      title: 'Serviços',
      headerStyle: {
        backgroundColor: colors.primary,
      },
      headerTintColor: colors.secondary
    };
  };

  render = () => {
    return null;
  }
}