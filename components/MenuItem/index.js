/**
 *
 * A link that receives a icon's name, label and a function that is triggered when the link is pressed  
 *
 * @summary Simple link with a given action
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import colors from '../../styles/colors';
import styles from './styles';

const MenuItem = ({ iconName, label, action, size }) => {
  
  if(!!iconName && !!label) {
    return (
      <View style={styles.container}>
        {/* A label with given icon and on press triggers the action (if it exists) */}
        <TouchableOpacity onPress={action}>
          <View style={styles.button}>
            <FontAwesome
              name={iconName}
              color={colors.primary}
              size={size}
              />
            <Text style={styles.menuItemText}>{label}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
  else {
    return null;
  }
};

export default MenuItem;