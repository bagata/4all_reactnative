/**
 *
 * The number received by this component is treated like an collection of stars
 * The number minus the MAX_RATING tells the collection of stars that are missing
 *
 * @summary Turns a number in to a array of stars
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import { View } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import styles from './styles';

const MAX_RATING = 5;

const Rating = ({ stars, color }) => {
  
  /**
   *  List of star icons to render
   */
  renderStars = () => {
    const arrayStars = [];
    if(!!stars) {
      // Fills array with star icons
      for(index=0; index<stars; index++) {
        arrayStars.push(
          <FontAwesome
          key={index+'star'}
          name={'star'}
          color={color}
          size={12}
          style={styles.starIcon}
          />
        )
      }
      const missingStars = MAX_RATING - stars;

      // Fills array with empty star icons
      for(index=0; index<missingStars; index++) {
        arrayStars.push(
          <FontAwesome
          key={(index+missingStars)+'star-o'}
          name={'star-o'}
          color={color}
          size={12}
          style={styles.starIcon}
          />
        )
      }
    }
    return arrayStars;
  }

  return (
    <View style={styles.container}>
      {this.renderStars()}
    </View>
  );
};

export default Rating;