/**
 *
 * This is the initial screen of the app
 *  List of places by id, each place leads to a detail screen
 *
 * @summary List of ids
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import { FlatList, TouchableOpacity, ActivityIndicator, Text, View, StyleSheet } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import colors from '../../styles/colors';
import styles from './styles';

export default class ListScreen extends React.Component {

  // Header Options
  static navigationOptions = {
    title: 'Lista de Lugares',
    headerStyle: {
      backgroundColor: colors.primary,
    },
    headerTintColor: colors.secondary
  };

  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      places: []
    }
  }

  componentDidMount = () => {
    this.getPlaces();
  }

  /**
   * Request to get list of places (JSON)
   */
  getPlaces = () => {
    try {
      fetch('http://dev.4all.com:3003/tarefa')
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          isLoading: false,
          places: responseJson.lista,
        });
      })
      .catch((error) => {
        this.setState({
          isLoading: false
        });
        console.error('Request failed', error);
      });
    }
    catch (error) {
      console.error('Not able to get list of places', error);      
    }
  }

  render = () => {
    // Shows loading spinner while request is being made
    if(this.state.isLoading) {
      return <ActivityIndicator size="large" color={colors.primary} style={styles.activityIndicator} />
    }
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.places}
          renderItem={({item}) => (
            // On press navigates to detail screen ta contains all data from this place
            <TouchableOpacity onPress={() => 
              this.props.navigation.push('Details', {
                id: item,
              })
            }>
            {/* List of places by id */}
            <View style={styles.wrap}>
              <FontAwesome name={'map-marker'} color={colors.primary} size={20} />
              <Text style={styles.listItem}>{item}</Text>
            </View>
            </TouchableOpacity>
          )}
          keyExtractor={(item) => item}
        />
      </View>
    );
  }
}