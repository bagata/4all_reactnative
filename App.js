import React from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import ListScreen from './components/ListScreen';
import DetailsScreen from './components/DetailsScreen';
import ServicesScreen from './components/ServicesScreen';

/**
 * Sets navigation routes
 * List is the initial route
 */
const RootStack = createStackNavigator(
  {
    List: ListScreen,
    Details: DetailsScreen,
    Services: ServicesScreen,
  },
  {
    initialRouteName: 'List',
  }
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}