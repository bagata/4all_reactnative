/**
 * This file contains all colors used in the app project
 */
const colors = {
  primary: '#CB8A19',
  secondary: '#FFFFFF',
  alternative: '#F2F2F2',
  dark: '#5A5A5A',
};

export default colors;