# 4allApp

## Dependencies

- **node** v10.12.0
- **npm** 6.9.0
- **react-native** 0.59.8
- **react-native-cli** 2.0.1

### Packages
- react-native-dotenv
- react-native-fullwidth-image
- react-native-gesture-handler
- react-native-vector-icons
- react-navigation

### Install
1. npm install -g react-native-cli
2. npm install
3. react-native link
4. Create *.env* file in the root of the project and add the GOOGLE_MAPS_API_KEY value

### Run
- Start emulator
- For android app: react-native run-android
- For IOS app: react-native run-ios